#pragma once
#include "SDL.h"
#include "Fragment.hpp"

namespace rs {

struct Fragment;

class Drawing
{
public:
	Drawing() = delete;
	Drawing(const Drawing&) = delete;
	Drawing(const Drawing&&) = delete;
	~Drawing() = delete;

	auto operator = (const Drawing&) -> Drawing& = delete;
	auto operator = (const Drawing&&) -> Drawing&& = delete;

	static auto DrawTriangle(SDL_Surface& image, const Fragment& first, const Fragment& second, const Fragment& third) -> void;
	static auto DrawScanline(SDL_Surface& image, int x1, int x2, const Color3& c1, const Color3& c2, int y) -> void;
	static auto DrawLine(SDL_Surface& image, const Fragment& first, const Fragment& second) -> void;
	static auto DrawPoint(SDL_Surface& image, const Fragment& point) -> void;
};

template <typename T> int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

} // namespace rs