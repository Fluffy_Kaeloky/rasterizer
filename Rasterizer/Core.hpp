#pragma once

#include <SDL.h>
#include "Matrix.hpp"
#include "RsEnums.hpp"
#include "Color.hpp"

namespace rs {

class RsCore
{
public:
	RsCore() = delete;
	RsCore(const RsCore&) = delete;

	auto operator = (const RsCore&) -> RsCore& = delete;
	auto operator = (const RsCore&&) -> RsCore&& = delete;

	static auto Initialize(SDL_Surface*) -> void;
	static auto Shutdown() -> void;

private:
	RsCore(SDL_Surface*);
	~RsCore();

public:
	static RsCore* instance;

	SDL_Surface* buffer = nullptr;

	Mat4 view;
	Mat4 projection;

	RsDrawType drawType = RS_TRIANGLE;

	Color4 clearColor;
};

} // namespace rs