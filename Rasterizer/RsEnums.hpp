#pragma once

namespace rs {

enum RsDrawType
{
	RS_POINT,
	RS_LINE,
	RS_TRIANGLE,
	RS_QUAD
};

}