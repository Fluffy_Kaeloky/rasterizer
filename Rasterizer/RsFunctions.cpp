#include "RsFunctions.hpp"
#include "Core.hpp"
#include "Color.hpp"
#include <SDL.h>

namespace rs {

auto RsBegin(RsDrawType drawType) -> void
{
	RsCore::instance->drawType = drawType;
}

auto RsEnd() -> void
{
	throw new std::exception("Not implemented yet");
}

auto RsClearColor(int r, int g, int b, int a) -> void
{
	RsCore::instance->clearColor = Color4(r, g, b, a);
}

auto RsClear() -> void
{
	SDL_FillRect(RsCore::instance->buffer, nullptr, SDL_MapRGBA(RsCore::instance->buffer->format, RsCore::instance->clearColor.r, RsCore::instance->clearColor.g, RsCore::instance->clearColor.b, RsCore::instance->clearColor.a));
}

auto RsSwapBuffer() -> void
{
	SDL_Flip(RsCore::instance->buffer);
}

} // namespace rs