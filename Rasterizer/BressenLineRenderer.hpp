#pragma once

struct SDL_Surface;

namespace rs {

struct Fragment;

class BressenLineRenderer
{
public:
	BressenLineRenderer(const Fragment&, const Fragment&);

	auto DrawLine(SDL_Surface& image) -> void;
	auto StepOnY(SDL_Surface& image, Fragment& lastFragmentDrawn) -> bool;

private:
	int x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
	const int& x1, x2, y1, y2;
	const Fragment& first;
	const Fragment& second;
};

} // namespace rs