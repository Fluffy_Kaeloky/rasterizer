#include "Color.hpp"

namespace rs {

auto rs::Color3::Lerp(const Color3& a, const Color3& b, float c) -> Color3
{
	return Color3(a.r + (b.r - a.r) * c, a.b + (b.b - a.b) * c, a.g + (b.g - a.g) * c);
}

} // namespace rs