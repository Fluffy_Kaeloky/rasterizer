#include "Drawing.hpp"
#include "BressenLineRenderer.hpp"
#include <math.h>

///Test
#include "RsFunctions.hpp"

namespace rs {

auto Drawing::DrawTriangle(SDL_Surface& image, const Fragment& first, const Fragment& second, const Fragment& third) -> void
{
	const Fragment* vtx[3] = { &first, &second, &third };
	int order[3] = { 0, 1, 2 };

	// find the lowest Y
	if (vtx[0]->y < vtx[1]->y)
	{
		if (vtx[0]->y < vtx[2]->y)
			order[0] = 0;
		else
			order[0] = 2;
	}
	else
	{
		if (vtx[1]->y < vtx[2]->y)
			order[0] = 1;
		else
			order[0] = 2;
	}

	// find the highest Y
	if (vtx[0]->y > vtx[1]->y)
	{
		if (vtx[0]->y > vtx[2]->y)
			order[2] = 0;
		else
			order[2] = 2;
	}
	else
	{
		if (vtx[1]->y > vtx[2]->y)
			order[2] = 1;
		else
			order[2] = 2;
	}

	order[1] = 3 - (order[0] + order[2]);

	//float d0 = (vtx[order[1]]->x - vtx[order[0]]->x) / (vtx[order[1]]->y - vtx[order[0]]->y);
	//float d1 = (vtx[order[2]]->x - vtx[order[0]]->x) / (vtx[order[2]]->y - vtx[order[0]]->y);


	///Top Half

	//         A
	//       /   \
	//this >/     \
	//     /     __B
	//    C _____

	bool changed;
	int x = vtx[order[0]]->x;
	int y = vtx[order[0]]->y;
	int dx = abs(vtx[order[2]]->x - vtx[order[0]]->x);
	int dy = abs(vtx[order[2]]->y - vtx[order[0]]->y);
	int signx = sgn(abs(vtx[order[2]]->x - vtx[order[0]]->x));
	if (dy > dx)
	{
		int t = dx;
		dx = dy;
		dy = t;
		changed = true;
	}
	float e = 2 * dy - dx;
	for (int i = 1; i <= dx; i++)
	{
		//plot(x, y);
		Color3 interpColor = Color3::Lerp(vtx[order[0]]->color, vtx[order[2]]->color, (float)i / (float)dx);
		DrawPoint(image, Fragment(x, y, interpColor));

		while (e >= 0)
		{
			if (changed)
				x = x + 1;
			else
				y = y + 1;
			e = e - 2 * dx;
		}
		if (changed)
			y += 1;
		else
			x += signx;
		e = e + 2 * dy;
	}
}

auto Drawing::DrawScanline(SDL_Surface& image, int x1, int x2, const Color3& c1, const Color3& c2, int y) -> void
{
}

auto Drawing::DrawLine(SDL_Surface& image, const Fragment& first, const Fragment& second) -> void
{
	BressenLineRenderer lineRenderer(first, second);
	lineRenderer.DrawLine(image);
}

auto Drawing::DrawPoint(SDL_Surface& image, const Fragment& point) -> void
{
	Uint32* pixels = (Uint32*)image.pixels;
	pixels[point.y * image.w + point.x] = SDL_MapRGB(image.format, point.color.r, point.color.g, point.color.b);
	RsSwapBuffer();
}

} // namespace rs