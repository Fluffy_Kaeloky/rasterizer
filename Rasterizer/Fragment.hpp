#pragma once
#include "Color.hpp"

namespace rs {

struct Fragment
{
public:
	inline Fragment(int x, int y, const Color3& color) { this->x = x; this->y = y; this->color = color; }
	int x, y;
	Color3 color;
};

} // namespace rs
