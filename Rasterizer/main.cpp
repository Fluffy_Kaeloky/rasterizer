#include <iostream>
#include <stdlib.h>
#include <SDL.h>
#undef main
#include "Matrix.hpp"
#include "RsFunctions.hpp"

///Test
#include "Core.hpp"
#include "Drawing.hpp"

auto main(int argc, char* argv[]) -> int
{
	rs::Mat4 test;

	std::cout << test << std::endl;
	std::cout << "[1,1] = " << test[1][1] << std::endl;
	std::cout << "[1,0] = " << test[1][0] << std::endl;

	rs::Mat4 test2;

	std::cout << "Identity * Identity = " << std::endl;
	std::cout << test.Multiply(test2) << std::endl;

	test2[0][3] = 2.0f;
	test2[1][3] = 2.0f;

	std::cout << "Identity * Tr X = 2 + Tr Y = 2" << std::endl;
	std::cout << test.Multiply(test2);

	SDL_Init(SDL_INIT_VIDEO);

	SDL_Surface* surf = SDL_SetVideoMode(800, 600, 32, SDL_HWSURFACE);
	SDL_WM_SetCaption("Rasterizer", nullptr);

	rs::RsCore::Initialize(surf);

	rs::RsClearColor(70, 130, 180, 255);

	bool quit = false;
	SDL_Event e;
	while (!quit)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
				quit = true;
		}

		rs::RsClear();

		///Test
		rs::Drawing::DrawPoint(*rs::RsCore::instance->buffer, rs::Fragment(400, 400, rs::Color3(255, 255, 255)));

		rs::Drawing::DrawLine(*rs::RsCore::instance->buffer, rs::Fragment(20, 20, rs::Color3(255, 0, 0)), rs::Fragment(200, 200, rs::Color3(0, 255, 0)));

		rs::Drawing::DrawTriangle(*rs::RsCore::instance->buffer, rs::Fragment(400, 300, rs::Color3(255, 0, 0)),
			rs::Fragment(500, 400, rs::Color3(0, 255, 0)),
			rs::Fragment(450, 500, rs::Color3(0, 0, 255)));
		///End Test

		rs::RsSwapBuffer();
	}

	rs::RsCore::Shutdown();

	SDL_FreeSurface(surf);

	SDL_Quit();

	return EXIT_SUCCESS;
}