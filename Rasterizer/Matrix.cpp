#include "Matrix.hpp"

namespace rs {

Mat4::Mat4()
{
	for (int c = 0; c < 4; c++)
	{
		for (int l = 0; l < 4; l++)
			values[l][c] = (c == l ? 1.0f : 0.0f);
	}
}

auto Mat4::ToString() const -> std::string
{
	std::string str;

	for (int c = 0; c < 4; c++)
	{
		for (int l = 0; l < 4; l++)
		{
			str += std::to_string(values[l][c]);
			if (l == 3)
				str += "\n";
			else
				str += ", ";
		}
	}

	return str;
}

auto Mat4::Multiply(const Mat4& other) const -> Mat4
{
	Mat4 out;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			float sum = 0.0f;

			for (int k = 0; k < 4; k++)
				sum += values[k][i] * other.values[k][j];

			out.values[j][i] = sum;
		}
	}

	return out;
}


auto Mat4::operator[] (int i) -> float*
{
	return values[i];
}

auto Mat4::operator[] (int i) const -> const float*
{
	return values[i];
}

auto Mat4::operator* (const Mat4& other) const -> Mat4
{
	return Multiply(other);
}

} // namespace rs

auto operator << (std::ostream& stream, const rs::Mat4& value) -> std::ostream&
{
	stream << value.ToString();

	return stream;
}