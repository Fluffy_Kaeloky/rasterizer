#include "Vector.hpp"
#include <string>

namespace rs {

Vector3f::Vector3f()
{
	x = y = z = 0.0f;
}

auto Vector3f::operator[] (int i) const -> const float&
{
	return *((&x) + i);
}

auto Vector3f::operator[] (int i) -> float&
{
	return *((&x) + i);
}

} // namespace rs

auto operator << (std::ostream& s, const rs::Vector3f& value) -> std::ostream&
{
	s << "(" << value.x << ", " << value.y << ", " << value.z << ")";
	return s;
}