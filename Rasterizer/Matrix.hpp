#pragma once
#include <iostream>
#include <string>

namespace rs {

class Mat4
{
public:
	Mat4();

	auto ToString() const -> std::string;

	auto Multiply(const Mat4& other) const -> Mat4;

	auto operator[] (int) -> float*;
	auto operator[] (int) const -> const float*;

	auto operator* (const Mat4& other) const -> Mat4;

private:
	float values[4][4];
};

} // namespace rs

auto operator << (std::ostream& stream, const rs::Mat4& value) -> std::ostream&;
