#include "Core.hpp"

namespace rs {

RsCore* RsCore::instance = nullptr;

auto RsCore::Initialize(SDL_Surface* surface) -> void
{
	if (RsCore::instance == nullptr)
	{
		instance = new RsCore(surface);
	}
}

auto RsCore::Shutdown() -> void
{
	if (RsCore::instance != nullptr)
	{
		delete instance;
		instance = nullptr;
	}
}

RsCore::RsCore(SDL_Surface* surface)
{
	buffer = surface;
}

RsCore::~RsCore()
{
	buffer = nullptr;
}

} // namespace rs