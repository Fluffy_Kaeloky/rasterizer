#pragma once
#include <iostream>

namespace rs {

class Vector3f
{
public:
	Vector3f();

	auto operator[] (int) const -> const float&;
	auto operator[] (int) -> float&;

	float x, y, z;
};

} // namespace rs

auto operator << (std::ostream&, const rs::Vector3f&) -> std::ostream&;