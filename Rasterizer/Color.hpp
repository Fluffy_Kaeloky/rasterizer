#pragma once

namespace rs {

struct Color3
{
public:
	Color3() = default;
	inline Color3(int r, int g, int b) { this->r = r; this->g = g; this->b = b; }
	int r, g, b;

	static auto Lerp(const Color3& a, const Color3& b, float c)->Color3;
};

struct Color4
{
public:
	Color4() = default;
	inline Color4(int r, int g, int b, int a = 255) { this->r = r; this->g = g; this->b = b; this->a = a; }
	int r, g, b, a = 255;
};

} // namespace rs