#include "BressenLineRenderer.hpp"
#include "Fragment.hpp"
#include "Color.hpp"
#include <SDL.h>

//Test
#include "RsFunctions.hpp"

namespace rs {

BressenLineRenderer::BressenLineRenderer(const Fragment& f, const Fragment& s): first(f), 
second(s),
x1(f.x),
x2(s.x),
y1(f.y),
y2(s.y)
{
	dx = x2 - x1;
	dy = y2 - y1;
	dx1 = abs(dx);
	dy1 = abs(dy);
	px = 2 * dy1 - dx1;
	py = 2 * dx1 - dy1;
}

auto BressenLineRenderer::DrawLine(SDL_Surface& image) -> void
{
	Uint32* pixels = (Uint32*)image.pixels;

	if (dy1 <= dx1)
	{
		if (dx >= 0)
		{
			x = x1;
			y = y1;
			xe = x2;
		}
		else
		{
			x = x2;
			y = y2;
			xe = x1;
		}

		RsSwapBuffer();
		pixels[y * image.w + x] = SDL_MapRGB(image.format, first.color.r, first.color.g, first.color.b);

		for (i = 0; x < xe; i++)
		{
			x = x + 1;
			if (px < 0)
				px = px + 2 * dy1;
			else
			{
				if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0))
					y = y + 1;
				else
					y = y - 1;
				px = px + 2 * (dy1 - dx1);
			}

			RsSwapBuffer();
			Color3 interpColor = Color3::Lerp(first.color, second.color, (float)x / (float)xe);
			pixels[y * image.w + x] = SDL_MapRGB(image.format, interpColor.r, interpColor.g, interpColor.b);
		}
	}
	else
	{
		if (dy >= 0)
		{
			x = x1;
			y = y1;
			ye = y2;
		}
		else
		{
			x = x2;
			y = y2;
			ye = y1;
		}

		RsSwapBuffer();
		pixels[y * image.w + x] = SDL_MapRGB(image.format, first.color.r, first.color.g, first.color.b);

		for (i = 0; y < ye; i++)
		{
			y = y + 1;
			if (py <= 0)
				py = py + 2 * dx1;
			else
			{
				if ((dx < 0 && dy < 0) || (dx > 0 && dy > 0))
					x = x + 1;
				else
					x = x - 1;
				py = py + 2 * (dx1 - dy1);
			}

			RsSwapBuffer();
			Color3 interpColor = Color3::Lerp(first.color, second.color, (float)y / (float)ye);
			pixels[y * image.w + x] = SDL_MapRGB(image.format, interpColor.r, interpColor.g, interpColor.b);
		}
	}
}

auto BressenLineRenderer::StepOnY(SDL_Surface& image, Fragment& lastFragmentDrawn) -> bool
{
	return true;
}

} // namespace rs
