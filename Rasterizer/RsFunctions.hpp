#pragma once

#include "RsEnums.hpp"

namespace rs {

auto RsBegin(RsDrawType drawType) -> void;
auto RsEnd() -> void;

auto RsClearColor(int r, int g, int b, int a) -> void;
auto RsClear() -> void;

auto RsSwapBuffer() -> void;

} // namespace rs